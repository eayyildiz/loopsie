﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    public PlayerController playerController;

    private int level = 0;

    // Use this for initialization
    void Start()
    {
        playerController.PlayerLevelFinishedHandler += onPlayerFinishLevel;
        playerController.PlayerDiedHandler += onPlayerDied;

        level = PlayerPrefs.GetInt("Level", 1);
        ChangeColors();
    }

    private void onPlayerDied(object sender, EventArgs e)
    {
        PlayerPrefs.SetInt("Level", 1);
        Invoke("LoadDeadScene", 1f);
    }

    private void onPlayerFinishLevel(object sender, EventArgs e)
    {
        SceneManager.LoadScene("EndScene");
    }

    public static Color GetColorForLevel(int level)
    {
        var rand = new System.Random(level * 8 + 65536);
        var green = rand.Next(128, 255);
        var red = rand.Next(128, 255);
        var blue = rand.Next(128, 255);

        return new Color((float)red / 255, (float)green / 255, (float)blue / 255);
    }

    private void ChangeColors()
    {
        var color = GetColorForLevel(level);

        RenderSettings.fogColor = color;
        Camera.main.backgroundColor = color;
    }

    public void LoadDeadScene()
    {
        SceneManager.LoadScene("GameOverScene");
    }
}
