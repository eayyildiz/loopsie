﻿using System;
using UnityEngine;

public class TransformConverter
{
    private readonly Vector3 worldDirection = Vector3.forward;

    private readonly Point origin;
    private readonly Vector3 direction;

    private float cosTeta;
    private float sinTeta;
    private Vector3 locationTransformVector;

    private float cosTeta2;
    private float sinTeta2;
    private Vector3 locationTransformVector2;

    public TransformConverter(Point origin, Vector3 direction)
    {
        this.origin = origin;
        this.direction = direction;

        CalculateTransformValues();
    }

    private void CalculateTransformValues()
    {
        cosTeta = Mathf.Round(((worldDirection.x * direction.x) + (worldDirection.z * direction.z))
                  / (Mathf.Pow(worldDirection.x, 2) + Mathf.Pow(worldDirection.z, 2)));

        sinTeta = Mathf.Round(((worldDirection.z * direction.x) - (worldDirection.x * direction.z))
                  / (Mathf.Pow(worldDirection.x, 2) + Mathf.Pow(worldDirection.z, 2)));

        cosTeta2 = Mathf.Round(((direction.x * worldDirection.x) + (direction.z * worldDirection.z))
                    / (Mathf.Pow(direction.x, 2) + Mathf.Pow(direction.z, 2)));

        sinTeta2 = Mathf.Round(((direction.z * worldDirection.x) - (direction.x * worldDirection.z))
                    / (Mathf.Pow(direction.x, 2) + Mathf.Pow(direction.z, 2)));

        locationTransformVector = new Vector3(origin.X,
                                                0,
                                                origin.Y);

        locationTransformVector2 = new Vector3(-origin.X * cosTeta2 + -origin.Y * sinTeta2,
                                                0,
                                                -origin.Y * cosTeta2 - -origin.X * sinTeta2);

    }

    public Vector3 WorldToMapDirection(Vector3 worldVector)
    {
        return new Vector3(worldVector.x * cosTeta2 + worldVector.z * sinTeta2,
                            0,
                            worldVector.z * cosTeta2 - worldVector.x * sinTeta2);
                    
    }

    public Vector3 MapToWorldDirection(Vector3 mapVector)
    {
        return new Vector3(mapVector.x * cosTeta + mapVector.z * sinTeta,
                            0,
                            mapVector.z * cosTeta - mapVector.x * sinTeta);
    }

    public Point WorldToMapPoint(Point worldPoint)
    {
        return new Point((int)(worldPoint.X * cosTeta2 + worldPoint.Y * sinTeta2 + locationTransformVector2.x),
                         (int)(worldPoint.Y * cosTeta2 - worldPoint.X * sinTeta2 + locationTransformVector2.z));
    }

    public Point MapToWorldPoint(Point mapPoint)
    {
        return new Point((int)(mapPoint.X * cosTeta + mapPoint.Y * sinTeta + locationTransformVector.x),
                         (int)(mapPoint.Y * cosTeta - mapPoint.X * sinTeta + locationTransformVector.z));
        
    }
}
