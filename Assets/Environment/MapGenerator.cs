﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator
{
    public Dictionary<Point, Tile> Map { get; } = new Dictionary<Point, Tile>();

    private System.Random random;

    private int maxDepth = 0;
    private int seed = 0;
    private int conflictCount = 0;

    public Point leftBottom { get; private set; } = new Point(0, 0);
    public Point rightTop { get; private set; } = new Point(0, 0);

    public MapGenerator(int seed)
    {
        this.seed = seed;
        random = new System.Random(seed + 65536);
    }

    public void Generate(int depth = 5)
    {
        maxDepth = depth;

        Map.Clear();
        var initialChunk = new Tile()
        {
            ForwardAvailable = true,
            Direction = new Vector2Int(0, 1)
        };

        Map.Add(new Point(0, 0), initialChunk);
        leftBottom = new Point(0, 0);
        rightTop = new Point(0, 0);
        GenerateForDepth(1, new Point(0, 1), new Vector2Int(0, 1), false);
    }

    private void GenerateForDepth(int depth, Point point, Vector2Int direction, bool teleportToStart)
    {
        var chunk = new Tile()
        {
            LeftAvailable = random.Next(10) > 7,
            RightAvailable = random.Next(10) > 7,
            ForwardAvailable = random.Next(10) > 4,
            LastTile = !teleportToStart && depth == maxDepth,
            TeleportToStart = teleportToStart,
            Direction = direction
        };
        if (!chunk.LeftAvailable && !chunk.RightAvailable)
        {
            chunk.ForwardAvailable = true;
        }

        if (!Map.ContainsKey(point))
        {
            if (point.X < leftBottom.X) 
            {
                leftBottom = new Point(point.X, leftBottom.Y);
            }
            if (point.X > rightTop.X)
            {
                rightTop = new Point(point.X, leftBottom.Y);
            }
            if (point.Y < leftBottom.Y)
            {
                leftBottom = new Point(leftBottom.X, point.Y);
            }
            if (point.Y > rightTop.Y)
            {
                rightTop = new Point(rightTop.X, point.Y);
            }

            Map.Add(point, chunk);
        }
        else
        {
            random = new System.Random(seed + 32768 + ++conflictCount);
            Generate(maxDepth);
        }

        if (chunk.LastTile || chunk.TeleportToStart)
            return;

        var availableDirections = new List<string>();
        if (chunk.RightAvailable) availableDirections.Add("Right");
        if (chunk.ForwardAvailable) availableDirections.Add("Forward");
        if (chunk.LeftAvailable) availableDirections.Add("Left");
        string breakTile = "";

        if (availableDirections.Count > 0)
        {
            int breakTileIndex = random.Next(1, availableDirections.Count);
            breakTile = availableDirections[breakTileIndex-1];
        }

        if (chunk.RightAvailable)
        {
            var nextVector = TurnClockWise(direction);
            var nextPoint = new Point(point.X + nextVector.x, point.Y + nextVector.y);
            if (breakTile == "Right")
            {
                GenerateForDepth(depth + 1, nextPoint, nextVector, false);
            }
            else
            {
                GenerateForDepth(depth + 1, nextPoint, nextVector, true);
            }
        }
        if (chunk.LeftAvailable)
        {
            var nextVector = TurnCounterClockwise(direction);
            var nextPoint = new Point(point.X + nextVector.x, point.Y + nextVector.y);
            if (breakTile == "Left")
            {
                GenerateForDepth(depth + 1, nextPoint, nextVector, false);
            }
            else
            {
                GenerateForDepth(depth + 1, nextPoint, nextVector, true);
            }
        }

        if (chunk.ForwardAvailable)
        {
            var nextPoint = new Point(point.X + direction.x, point.Y + direction.y);
            if (breakTile == "Forward")
            {
                GenerateForDepth(depth + 1, nextPoint, direction, false);
            }
            else
            {
                GenerateForDepth(depth + 1, nextPoint, direction, true);
            }
        }
    }

    public static Vector2Int TurnClockWise(Vector2Int vector)
    {
        return new Vector2Int(vector.y, -1 * vector.x);
    }

    public static Vector2Int TurnCounterClockwise(Vector2Int vector)
    {
        return new Vector2Int(-1 * vector.y, vector.x);
    }

}
