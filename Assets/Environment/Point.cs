﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point
{
    public int X { get; }
    public int Y { get; }

    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    public override bool Equals(object obj)
    {
        if (obj is Point point)
        {
            return (X == point.X) && (Y == point.Y);
        }
        return false;
    }

    public override int GetHashCode()
    {
        return X.GetHashCode() ^ Y.GetHashCode();
    }

    public override string ToString()
    {
        return $"({X},{Y})";
    }

}
