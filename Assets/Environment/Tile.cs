﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public bool LeftAvailable { get; set; }
    public bool RightAvailable { get; set; }
    public bool ForwardAvailable { get; set; }
    public bool LastTile { get; set; }
    public bool TeleportToStart { get; set; }
    public Vector2Int Direction { get; set; }

}
