﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    public GameObject StartTile;
    public GameObject EndTile;
    public GameObject StraightTile;
    public GameObject LeftTile;
    public GameObject RightTile;
    public GameObject StraightLeftTile;
    public GameObject StraightRightTile;
    public GameObject CrossingTile;
    public GameObject TCrossingTile;
    public GameObject ReSpawnerTile;
    public GameObject WallTile;
    public PlayerController playerController;

    private MapGenerator mapGenerator;
    private Dictionary<Point, GameObject> pool;
    private TransformConverter transformConverter;

    public TransformConverter TransformConverter => transformConverter;
    public Point LeftBottom => mapGenerator.leftBottom;
    public Point RightTop => mapGenerator.rightTop;

    private void Awake()
    {
        var level = PlayerPrefs.GetInt("Level", 1);

        pool = new Dictionary<Point, GameObject>();
        transformConverter = new TransformConverter(new Point(0, 0), Vector3.forward);
        mapGenerator = new MapGenerator(level);
        mapGenerator.Generate(10 + level / 5);

        playerController.PlayerMoveEventHandler += onPlayerMove;
        playerController.PlayerStuckedInLoopHandler += onPlayerStuckedInLoop;
    }

    private void onPlayerStuckedInLoop(object sender, PlayerStuckedInLoopHandlerArgs e)
    {
        transform.forward = e.Direction;
        transform.position = new Vector3(e.WorldPoint.X * 16, 0, e.WorldPoint.Y * 16);
        transformConverter = new TransformConverter(e.WorldPoint, e.Direction);
        SpawnTile(new Point(0, 0));
    }

    public void onPlayerMove(object sender, PlayerMoveEventHandlerArgs args)
    {
        var playerPoint = transformConverter.WorldToMapPoint(args.Point);

        SpawnForTile(playerPoint);
        DeactivateTiles(playerPoint);
    }

    void Start()
    {
        var origin = new Point(0, 0);
        pool.Add(origin, Instantiate(StartTile, new Vector3(0, 0, 0), GetQuaternion(Vector2Int.up)));
        pool[origin].transform.SetParent(transform);
        SpawnForTile(origin);
    }

    public Tile GetTile(Point worldPoint)
    {
        var point = transformConverter.WorldToMapPoint(worldPoint);
        Tile response;
        if (mapGenerator.Map.TryGetValue(point, out response))
        {
            return response;
        }
        return null;
    }

    public void SpawnForTile(Point mapPoint)
    {
        if (mapGenerator.Map.TryGetValue(mapPoint, out Tile currentTile))
        {
            if (currentTile.LeftAvailable)
            {
                var turnVector = MapGenerator.TurnCounterClockwise(currentTile.Direction);
                SpawnTile(new Point(mapPoint.X + turnVector.x, mapPoint.Y + turnVector.y));
            }
            if (currentTile.RightAvailable)
            {
                var turnVector = MapGenerator.TurnClockWise(currentTile.Direction);
                SpawnTile(new Point(mapPoint.X + turnVector.x, mapPoint.Y + turnVector.y));
            }
            if (currentTile.ForwardAvailable)
            {
                var turnVector = currentTile.Direction;
                SpawnTile(new Point(mapPoint.X + turnVector.x, mapPoint.Y + turnVector.y));
            }
        }

    }

    public void SpawnTile(Point point)
    {
        if (pool.ContainsKey(point))
        {
            if (point.X == 0 && point.Y == 0)
            {
                pool[point].SetActive(true);
            }
            else
            {
                pool[point].GetComponent<TileController>().StartSpawn();
            }
        }
        else if (mapGenerator.Map.TryGetValue(point, out Tile tileObject))
        {
            var chunkGameObject = GetGameObjectByChunk(tileObject);
            var pos = GetPosition(point);
            pool.Add(point, Instantiate(chunkGameObject, GetPosition(point), GetQuaternion(tileObject.Direction)));
            pool[point].transform.SetParent(transform);
            pool[point].GetComponent<TileController>().StartSpawn();
            if (tileObject.TeleportToStart) SpawnRespawner(pos);
        }

    }

    void SpawnRespawner(Vector3 pos)
    {
        var go = Instantiate(ReSpawnerTile, pos, Quaternion.identity);
        go.transform.SetParent(transform);
    }

    void DeactivateTiles(Point mapPoint)
    {
        foreach (Point point in pool.Keys)
        {
            float magnitude = (new Vector2(point.X, point.Y) - new Vector2(mapPoint.X, mapPoint.Y)).magnitude;
            if (magnitude > 1)
            {
                pool[point].GetComponent<TileController>().StartDrop();
            }

        }
    }

    private Vector3 GetPosition(Point point)
    {
        point = transformConverter.MapToWorldPoint(point);
        return new Vector3(point.X * 16, 0, point.Y * 16);
    }

    private Point GetPoint(Vector3 vector)
    {
        return new Point((int)vector.x / 16, (int)vector.z / 16);
    }

    private Quaternion GetQuaternion(Vector2Int direction)
    {
        var originalRotation = new Vector3(0, 0, 0);
        var targetDirection = transformConverter.MapToWorldDirection(new Vector3(direction.x, 0, direction.y));

        var quaternion = Quaternion.LookRotation(targetDirection, Vector3.up);

        return Quaternion.Euler(originalRotation.x, quaternion.eulerAngles.y, originalRotation.z);

    }

    private GameObject GetGameObjectByChunk(Tile tile)
    {
        if (tile.TeleportToStart)
            return StraightTile;
        if (tile.LastTile)
            return EndTile;

        if (tile.ForwardAvailable && !tile.LeftAvailable && !tile.RightAvailable)
            return StraightTile;

        if (!tile.ForwardAvailable && tile.LeftAvailable && !tile.RightAvailable)
            return LeftTile;

        if (!tile.ForwardAvailable && !tile.LeftAvailable && tile.RightAvailable)
            return RightTile;

        if (!tile.ForwardAvailable && tile.LeftAvailable && tile.LeftAvailable)
            return TCrossingTile;

        if (tile.ForwardAvailable && tile.LeftAvailable && !tile.RightAvailable)
            return StraightLeftTile;

        if (tile.ForwardAvailable && !tile.LeftAvailable && tile.RightAvailable)
            return StraightRightTile;

        if (tile.ForwardAvailable && tile.LeftAvailable && tile.RightAvailable)
            return CrossingTile;

        return StraightTile;
    }

}
