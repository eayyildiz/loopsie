﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : MonoBehaviour
{
    private Vector3 originalPosition;
    private Vector3 startPosition;
    private Vector3 dropPosition;

    private bool isSpawning = false;
    private bool isDropping = false;
    private bool firstSpawn = true;

    void Update()
    {
        if (isSpawning) {
            transform.localPosition = Vector3.Lerp(transform.localPosition, originalPosition, 5 * Time.deltaTime);
            if (transform.localPosition.y - originalPosition.y < 0.1f) {
                transform.localPosition = originalPosition;
                isSpawning = false;
            }
        }
    }

    public void StartSpawn() 
    {
        if (firstSpawn)
        {
            originalPosition = transform.localPosition;
            startPosition = new Vector3(originalPosition.x, originalPosition.y + 5, originalPosition.z);
            firstSpawn = false;
        }
        gameObject.SetActive(true);
        transform.localPosition = startPosition;
        isSpawning = true;
    }

    public void StartDrop() 
    {
        gameObject.SetActive(false);
    }

}
