﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevelController : MonoBehaviour
{
    public GameObject player;

    private Animator animator;

    // Start is called before the first frame update
    private void Start()
    {
        animator = player.GetComponent<Animator>();
        animator.SetBool("isJumping", true);
        player.GetComponent<Rigidbody>().AddForce(Vector3.up * 10, ForceMode.Impulse);

        Invoke("StopPlayerJump", 2f);

        var level = PlayerPrefs.GetInt("Level", 1);
        Camera.main.backgroundColor = GameController.GetColorForLevel(level - 1);
    }
    public void NextLevelClicked()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void MainMenuClicked()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void StopPlayerJump() 
    {
        animator.SetBool("isJumping", false);
    }
}
