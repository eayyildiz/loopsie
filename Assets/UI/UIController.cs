﻿using Assets.Player;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text LoopText;
    public Text RestartText;
    public Text CoinText;
    public Text HealthText;

    public PlayerController playerController;

    private void Awake()
    {
        playerController.PlayerRespawnHandler += PlayerRespawned;
        playerController.PlayerCapturedCoinHandler += PlayerCapturedCoin;
        playerController.PlayerHealthChangedHandler += PlayerHealthChanged;
    }

    private void PlayerHealthChanged(object sender, PlayerHealthChangedHandlerArgs e)
    {
        HealthText.text = e.RemainingHealth.ToString();
        if(e.RemainingHealth >= 0 && !e.InitialHealth)
        {
            ShowRestartTextMessage();
            Invoke("HideRestartTextMessage", 1);
        }
    }

    private void PlayerCapturedCoin(object sender, PlayerCapturedCoinHandlerArgs e)
    {
        this.CoinText.text = e.TotalCoin.ToString();
    }

    private void PlayerRespawned(object sender, EventArgs e)
    {
        ShowRespawedTextMessage();
        Invoke("HideRespawedTextMessage", 2);
    }

    public void ShowRespawedTextMessage()
    {
        LoopText.gameObject.SetActive(true);
    }

    public void HideRespawedTextMessage()
    {
        LoopText.gameObject.SetActive(false);
    }

    public void ShowRestartTextMessage()
    {
        RestartText.gameObject.SetActive(true);
    }

    public void HideRestartTextMessage() 
    {
        RestartText.gameObject.SetActive(false);
    }

}
