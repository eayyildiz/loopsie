﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class MinimapController : MonoBehaviour
{
    public GameObject mapImage;
    private Texture2D texture;
    private Image image;
    private List<Point> exploredPoints = new List<Point>();
    private Point currentPoint = new Point(0,0);

    private Point canvasSize = new Point(25, 25);

    public PlayerController playerController;
    public TileSpawner tileSpawner;

    void Start()
    {
        playerController.PlayerMoveEventHandler += onPlayerMoved;
        image = mapImage.GetComponent<Image>();

        exploredPoints.Add(new Point(0, 0));
    }

    private void onPlayerMoved(object sender, PlayerMoveEventHandlerArgs e)
    {
        var tile = tileSpawner.GetTile(e.Point);
        if (tile == null) 
        {
            return;
        }

        currentPoint = tileSpawner.TransformConverter.WorldToMapPoint(e.Point);
        if (!exploredPoints.Contains(currentPoint) && !tile.TeleportToStart)
        {
            exploredPoints.Add(currentPoint);
        }

        texture = new Texture2D(canvasSize.X, canvasSize.Y, TextureFormat.ARGB32, false, false);
        foreach (Point p in exploredPoints)
        {
            texture.SetPixel(p.X - currentPoint.X + 12, p.Y - currentPoint.Y + 12, Color.gray);
        }
        texture.SetPixel(12, 12, Color.red);

        texture.Apply();

        Sprite s = Sprite.Create(texture, new Rect(0, 0, canvasSize.X, canvasSize.Y),
                                Vector2.zero, 1);

        image.sprite = s;
    }
}
