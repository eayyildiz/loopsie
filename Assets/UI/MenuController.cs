﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Assets.UI
{
    public class MenuController : MonoBehaviour
    {
        public Text levelText;
        // Use this for initialization
        void Start()
        {
            var level = PlayerPrefs.GetInt("Level",1);
            levelText.text = $"level - {level}";

            Camera.main.backgroundColor = GameController.GetColorForLevel(level);
        }

        public void PlayGameClicked()
        {
            SceneManager.LoadScene("GameScene");
        }
    }
}