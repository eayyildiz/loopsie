﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public GameObject coinPrefab;
    public GameObject endCoinPrefab;
    public TileSpawner tileSpawner;

    public List<Point> exploredTiles;

    public PlayerController playerController;

    private void Awake()
    {
        playerController.PlayerMoveEventHandler += onPlayerMove;
        exploredTiles = new List<Point>();
        exploredTiles.Add(new Point(0, 0));
    }

    private void onPlayerMove(object sender, PlayerMoveEventHandlerArgs e)
    {
        var tile = tileSpawner.GetTile(e.Point);
        var mapPoint = tileSpawner.TransformConverter.WorldToMapPoint(e.Point);
        if (!exploredTiles.Contains(mapPoint) && tile != null)
        {
            if (tile.LastTile)
            {
                Instantiate(endCoinPrefab, new Vector3(e.Point.X * 16, 0.5f, e.Point.Y * 16), Quaternion.identity);
            }
            else if (!tile.TeleportToStart)
            {
                Instantiate(coinPrefab, new Vector3(e.Point.X * 16, 0.5f, e.Point.Y * 16), Quaternion.identity);
            }
            exploredTiles.Add(mapPoint);
        }
    }
}
