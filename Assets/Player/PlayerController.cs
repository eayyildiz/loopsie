﻿using Assets.Player;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator animator;
    private new Rigidbody rigidbody;

    private Point mapPoint = new Point(0, 0);
    private Point lastTurnPoint = new Point(0, 0);
    private bool turnLeftRequested = false;
    private bool turnRightRequested = false;
    private int coinCount = 0;
    private int level = 0;
    private int health = 0;

    public TileSpawner tileSpawner;
    public AudioSource footStepSound;
    public AudioSource coinSound;
    public ParticleSystem loopParticles;

    public float speed;

    public EventHandler<PlayerMoveEventHandlerArgs> PlayerMoveEventHandler;
    public EventHandler<PlayerCapturedCoinHandlerArgs> PlayerCapturedCoinHandler;
    public EventHandler<PlayerHealthChangedHandlerArgs> PlayerHealthChangedHandler;
    public EventHandler<PlayerStuckedInLoopHandlerArgs> PlayerStuckedInLoopHandler;
    public EventHandler PlayerDiedHandler;
    public EventHandler PlayerRespawnHandler;
    public EventHandler PlayerLevelFinishedHandler;

    private bool isRunning;
    private Point respawnPoint = new Point(0,0);
    private Vector3 respawnForward = Vector3.forward;

    void Awake()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        loopParticles.Stop();
        level = PlayerPrefs.GetInt("Level", 1);
        health = PlayerPrefs.GetInt("Health", 3);

        Invoke("StartRun", 0.5f);
    }

    void Start()
    {
        PlayerHealthChangedHandler.Invoke(this, new PlayerHealthChangedHandlerArgs { RemainingHealth = health, InitialHealth = true });
    }

    // Update is called once per frame
    void Update()
    {
        if (SwipeInput.SwipedLeft)
        {
            TurnLeft();
        }
        if (SwipeInput.SwipedRight)
        {
            TurnRight();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Respawn")
        {
            loopParticles.Play();
            respawnPoint = GetPlayerPosition();
            respawnForward = transform.forward;
            PlayerStuckedInLoopHandler.Invoke(this,
                                                new PlayerStuckedInLoopHandlerArgs
                                                {
                                                    WorldPoint = respawnPoint,
                                                    Direction = transform.forward
                                                }
                                               );
           
            PlayerRespawnHandler.Invoke(this, null);
            PlayerMoveEventHandler.Invoke(this, new PlayerMoveEventHandlerArgs
                                                {
                                                    Point = respawnPoint
                                                });
            
            Invoke(nameof(StopLoopParticles), 1f);

        }
        if (other.gameObject.tag == "Coin")
        {
            Destroy(other.gameObject);
            coinCount++;
            coinSound.Play();
            PlayerCapturedCoinHandler.Invoke(this, new PlayerCapturedCoinHandlerArgs { TotalCoin = coinCount });
        }
        if (other.gameObject.tag == "Finish")
        {
            Destroy(other.gameObject);
            LevelUp();
        }
        if (other.gameObject.tag == "Wall")
        {
            animator.SetBool("isRunning", false);
            animator.SetBool("isJumping", true);
            health--;
            if (health >= 0)
            {
                Invoke("Respawn", 1f);
                PlayerHealthChangedHandler.Invoke(this, new PlayerHealthChangedHandlerArgs { RemainingHealth = health });
            }
            else
            {
                Die();
            }
        }
    }

    private void StopLoopParticles()
    {
        if (loopParticles != null) {
            loopParticles.Stop();
        }
    }

    private void Respawn()
    {
        transform.position = new Vector3(respawnPoint.X * 16, 0, respawnPoint.Y * 16);
        transform.rotation = Quaternion.LookRotation(respawnForward, Vector3.up);
        rigidbody.velocity = Vector3.zero;

        tileSpawner.SpawnTile(new Point(0, 0));
        animator.SetBool("isJumping", false);
        StartRun();
    }

    private void FixedUpdate()
    {
        Point point = GetPlayerPosition();
        if (!point.Equals(mapPoint))
        {
            mapPoint = point;
            PlayerMoveEventHandler.Invoke(this, new PlayerMoveEventHandlerArgs
            {
                Point = mapPoint
            });
        }

        var tile = tileSpawner.GetTile(mapPoint);
        var desiredTilePos = new Vector3(mapPoint.X * 16, 0, mapPoint.Y * 16);

        if ((transform.position - desiredTilePos).magnitude < 2.0f && !lastTurnPoint.Equals(point))
        {
            lastTurnPoint = point;
            if (turnLeftRequested && tile.LeftAvailable)
            {
                transform.position = new Vector3(desiredTilePos.x, transform.position.y, desiredTilePos.z);
                transform.Rotate(0.0f, -90.0f, 0.0f);
                rigidbody.velocity = Vector3.zero;
                turnLeftRequested = false;

            }
            if (turnRightRequested && tile.RightAvailable)
            {
                transform.position = new Vector3(desiredTilePos.x, transform.position.y, desiredTilePos.z);
                transform.Rotate(0.0f, 90.0f, 0.0f);
                rigidbody.velocity = Vector3.zero;
                turnRightRequested = false;
            }
        }

        if (isRunning)
            Run();
    }

    private Point GetPlayerPosition()
    {
        var xdiff = (transform.position.x > 0) ? 8 : -8;
        var zdiff = (transform.position.z > 0) ? 8 : -8;

        var point = new Point(((int)transform.position.x + xdiff) / 16, ((int)transform.position.z + zdiff) / 16);
        return point;
    }

    public void StopRun()
    {
        animator.SetBool("isRunning", false);
        footStepSound.Stop();
        isRunning = false;
    }

    public void StartRun()
    {
        animator.SetBool("isRunning", true);
        footStepSound.Play();
        isRunning = true;
    }

    public void Run()
    {
        rigidbody.AddForce(gameObject.transform.forward * speed, ForceMode.Impulse);
    }

    public void LevelUp()
    {
        PlayerPrefs.SetInt("Level", ++level);
        StopRun();
        PlayerLevelFinishedHandler.Invoke(this, null);
    }

    public void Die()
    {
        PlayerDiedHandler.Invoke(this, null);
    }

    public void TurnLeft()
    {
        turnLeftRequested = true;
        turnRightRequested = false;
    }

    public void TurnRight()
    {
        turnRightRequested = true;
        turnLeftRequested = false;
    }
}
