﻿using System;
using UnityEngine;

public class PlayerStuckedInLoopHandlerArgs : EventArgs
{
    public Point WorldPoint { get; set; }
    public Vector3 Direction { get; set; }
}