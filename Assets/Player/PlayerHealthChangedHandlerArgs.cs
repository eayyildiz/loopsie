﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Player
{
    public class PlayerHealthChangedHandlerArgs : EventArgs
    {
        public int RemainingHealth { get; set; }
        public bool InitialHealth { get; set; }
    }
}
