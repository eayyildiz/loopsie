﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Player
{
    public class PlayerCapturedCoinHandlerArgs : EventArgs
    {
        public int TotalCoin { get; set; }
    }
}
