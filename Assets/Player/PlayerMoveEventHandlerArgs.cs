﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;

public class PlayerMoveEventHandlerArgs : EventArgs
{
    public Point Point { get; set; }
}

