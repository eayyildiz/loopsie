﻿using UnityEngine;

public class CameraFollowController : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform objectToFollow;
    public Vector3 offset;
    public float followSpeed = 5;
    public float lookSpeed = 5;
    public Vector3 velocity = Vector3.one;

    public void LateUpdate()
    {
        MoveToTarget();
        //SmoothFollow();
        LookAtTarget();
    }

    public void LookAtTarget() 
    {
        Vector3 lookDirection = objectToFollow.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(lookDirection, Vector3.up);

        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, lookSpeed * Time.deltaTime);
    }

    public void MoveToTarget()
    {
        Vector3 targetPosition = objectToFollow.position +
                                objectToFollow.forward * offset.z +
                                objectToFollow.right * offset.x +
                                objectToFollow.up * offset.y;

        transform.position = Vector3.Lerp(transform.position, targetPosition, followSpeed * Time.deltaTime);
    }

    void SmoothFollow() 
    {
        Vector3 targetPosition = objectToFollow.position +
                                objectToFollow.forward * offset.z +
                                objectToFollow.right * offset.x +
                                objectToFollow.up * offset.y;

        Vector3 curPos = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity , followSpeed * Time.deltaTime);
        transform.position = curPos;
    }
}

